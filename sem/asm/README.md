# asm

В этой задаче вам нужно будет реализовать несколько простых функций на ассемблере.

Тесты находятся в файле [`tests/asm.rs`](https://gitlab.com/sergey-v-galtsev/nikka-public/-/blob/master/sem/asm/tests/asm.rs).

Описания требуемых функций находятся в файле [`src/lib.rs`](https://gitlab.com/sergey-v-galtsev/nikka-public/-/blob/master/sem/asm/src/lib.rs).

Отлаживайте код в gdb с подключенным плагином [peda](https://github.com/longld/peda).

На сайте
[rust.godbolt.org](https://rust.godbolt.org/z/1PsT7jjnx)
можно посмотреть на ассемблерный код, который генерирует Rust.
