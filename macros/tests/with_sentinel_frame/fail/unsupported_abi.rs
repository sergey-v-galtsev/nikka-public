use macros::with_sentinel_frame;

#[with_sentinel_frame]
extern "Rust" fn test() {
}

fn main() {
}
