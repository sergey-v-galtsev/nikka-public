### Курс "Архитектура компьютера и операционные системы" в ШАД

- [Настройка окружения для сдачи задач](https://gitlab.com/sergey-v-galtsev/nikka-public/-/tree/main/SETUP.md).
- [Публичный репозиторий Nikka](https://gitlab.com/sergey-v-galtsev/nikka-public).
- [Задачи в manytask](https://os.manytask.org/).
- [Описание лаб](https://sergey-v-galtsev.gitlab.io/labs-description/lab/book/index.html).
